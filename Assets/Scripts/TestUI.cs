namespace Project2
{
    using Building;
    using Building.Settings;
    using UnityEngine;
    using Renderer = Building.Renderer;

    [RequireComponent(typeof(Renderer))]
    public class TestUI : MonoBehaviour
    {
        private Renderer _renderer;
        [SerializeField] private BuildingSettings settings;

        public void OnGenerateClicked()
        {
            _renderer.Render(Generator.Generate(settings));
        }

        public void Start()
        {
            _renderer = GetComponent<Renderer>();
        }
    }
}